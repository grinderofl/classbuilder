﻿namespace ClassBuilder
{
    public enum Accessors
    {
        Public,
        Private,
        Internal
    }
}
