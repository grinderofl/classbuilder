﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ClassBuilder
{
    /// <summary>
    /// Builds a class according to give parameters
    /// </summary>
    public class ClassBuilder
    {
        private readonly List<string> _implementations;

        private readonly Dictionary<string, Dictionary<Descriptors, string>> _methods;

        private readonly string _name;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of class to generate</param>
        public ClassBuilder(string name)
        {
            _implementations = new List<string>();
            _methods = new Dictionary<string, Dictionary<Descriptors, string>>();
            _name = name;
        }

        /// <summary>
        /// Add implementation to class
        /// </summary>
        /// <param name="implementation">Implementation name</param>
        /// <returns>ClassBuilder</returns>
        public ClassBuilder AddImplementation(string implementation)
        {
            if (string.IsNullOrEmpty(implementation))
                throw new ArgumentNullException("implementation");

            _implementations.Add(implementation);
            return this;
        }

        #region AddMethod overloads

        /// <summary>
        /// Add method to class
        /// </summary>
        /// <param name="methodname">Method name</param>
        /// <param name="type">Method type</param>
        /// <param name="contents">Method contents</param>
        /// <returns>ClassBuilder</returns>
        public ClassBuilder AddMethod(string methodname, string type, string contents)
        {
            return AddMethod(methodname, type, contents, Accessors.Public);
        }

        /// <summary>
        /// Add method to class
        /// </summary>
        /// <param name="methodname">Method name</param>
        /// <param name="type">Method type</param>
        /// <param name="contents">Method contents</param>
        /// <param name="accessor">Method accessor</param>
        /// <returns>ClassBuilder</returns>
        public ClassBuilder AddMethod(string methodname, string type, string contents, Accessors accessor)
        {
            return AddMethod(methodname, type, contents, accessor, null);
        }

        /// <summary>
        /// Add method to class
        /// </summary>
        /// <param name="methodname">Method name</param>
        /// <param name="type">Method type</param>
        /// <param name="contents">Method contents</param>
        /// <param name="accessor">Method accessor</param>
        /// <param name="parameters">Method parameters</param>
        /// <returns>ClassBuilder</returns>
        public ClassBuilder AddMethod(string methodname, string type, string contents, Accessors accessor, string parameters)
        {
            var dict = new Dictionary<Descriptors, string>
                           {
                               {Descriptors.Name, methodname},
                               {Descriptors.Type, type},
                               {Descriptors.Contents, contents},
                               {Descriptors.Accessor, accessor.ToString().ToLower()},
                               {Descriptors.Parameters, parameters}
                           };

            _methods.Add(methodname, dict);
            return this;
        }

        #endregion

        public override string ToString()
        {
            string template = _implementations.Count > 0
                                  ? ReadTemplate("ClassTemplateWithImplementation")
                                  : ReadTemplate("ClassTemplate");

            template = template.Replace("{CLASSNAME}", _name);
            template = template.Replace("{IMPLEMENTATION}", String.Join(", ", _implementations));

            string methods =
                _methods.Select(
                    item =>
                    ReadTemplate("MethodTemplate")
                    .Replace("{ACCESSOR}", item.Value[Descriptors.Accessor])
                    .Replace("{TYPE}", item.Value[Descriptors.Type])
                    .Replace("{METHODNAME}", item.Value[Descriptors.Name])
                    .Replace("{PARAMETERS}", item.Value[Descriptors.Parameters])
                    .Replace("{METHODCONTENTS}", item.Value[Descriptors.Contents])
                )
                .Aggregate(string.Empty, (current, method) => current + method);

            return template.Replace("{CLASSCONTENTS}", methods);
        }

        /// <summary>
        /// Read a template file
        /// </summary>
        /// <param name="name">File name</param>
        /// <returns>Template</returns>
        private string ReadTemplate(string name)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            Stream stream = asm.GetManifestResourceStream("ClassBuilder.Templates." + name + ".tpl");
            if (stream == null)
                throw new FileNotFoundException("name");

            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
