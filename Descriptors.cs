﻿namespace ClassBuilder
{
    public enum Descriptors
    {
        Accessor,
        Type,
        Name,
        Parameters,
        Contents
    }
}
